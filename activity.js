db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operation"
		},
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"email": "janedoe@mail.com",
			"department": "HR"
		},

	])

// ---------------------------------------------------

db.users.find(
		{ $or: [
			{"firstName": {$regex: 'S',$options: '$i'}}, {"lastName": {$regex: 'D', $options: '$i'}} 
			]
		}
	)
// -----------------------------------------------------------

db.users.find(
		{
			$and: [{"department": "HR"}, {"age": {$gt: 70}}]
		}

	)
// ---------------------------------------------------------------
db.users.find(
		{
			$and: [{"firstName": {$regex: 'E', $options: '$i'}}, {"age": {$lte: 30}}]
		}

	)
